# Первоначальная настройка файла .env #

## Режим работы приложения ##

Приложение может работать в двух режимах:

* Разработка (local)

```
APP_ENV=production
APP_DEBUG=false
```

* Рабочий вариант (production)

```
APP_ENV=local
APP_DEBUG=true
```

В режиме отладки будут отображаться все ошибки и их описание.


В рабочем режиме все ошибки будут заменены на сообщения о неисправностях (в стиле "что-то пошло не так")


*(выше и далее указанные строки куда нужно добавлять в свой файл .env для достижения того или иного эффекта работы приложения)*


## Адрес проекта ##

```
APP_URL=http://project.ru
```


Необходимо указать домен, по которому доступен проект. Будет подставляться в различные участки кода приложения.


## API-ключ приложения ##


Единовременно генерируется с помощью команды консоли в директории проекта:



```
php artisan key:generate
```

Переменная API_KEY автоматически добавляется в файл .env после выполнения команды.


## Настройки базы данных ##


```
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=root
DB_PASSWORD=root
```

Указываются настройки для существующей в окружении базы данных

## Базовые модули приложения ##

```
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync
```

Настройки типов кэша, хранения сессий, очередей. Можно оставить без изменений.


Настройки хранения кэша Redis



```
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
```

Указаны настройки по-умолчанию. Можно оставить без изменений.

## Настройки отправки почтовых сообщений ##

```
MAIL_DRIVER=mailgun
MAIL_HOST=smtp.mailgun.org
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAILGUN_DOMAIN=domen
MAILGUN_SECRET=secret
```

В laravel почтовые сообщения можно отправлять как через stmp, так и через сторонние сервисы (mailgun, Amazon SES).

Для локальной работы можно использовать **MAIL_DRIVER=log**
Функции отправки почти будут отрабатываться и записывать отправляемые сообщения в log-файл по адресу storage/log/laravel.log


## Настройки содержимого почтовых сообщений ##

```
MAIL_FROM=test@test.ru
MAIL_SUBJ="Message From Test Project"
```

Адрес сообщения FROM и тема для отправляемых сообщение соответственно.

Обратите внимание, что текст с пробелами должен указываться в кавычках.

## Google reCaptcha ##


```
RECAPTCHA_SITEKEY="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
RECAPTCHA_SECRET="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
```

Необходимые параметры для работы капчи на страницах проекта