<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Post;
use App\User;
use DB;
use App;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller 
{	
	public function index() {

		$header = "Последние записи пользователей";
		$posts = Post::orderBy('created_at','desc')
					   ->where('status','1')
					   ->paginate(5);

		return view('index',compact('posts','header'));
	}

	public function searchPage() {
		$query = "";
		return view('search',compact('query','results'));
	}

	public function searchResults(Request $request) {
		$query = $request['search'];		

		// Проверяем форму на ошибки

		if(empty($query)) {
			$error = "Пустой поисковый запрос";
		} else {
			if(strlen($query) < 4) {
				$error = "Поисковый запрос менее 4 символов";
			}
		}

		// Если были ошибки, не делаем запрос, возвращаем текст ошибки
		if(isset($error)) {
			return view('search',compact('query','error'));
		} else {
			$results = Post::where('title','like','%'.$query.'%')
						->orWhere('text','like','%'.$query.'%')
						->get();

			// Добавляем имя пользователя для формирования ссылки на публикацию

			foreach ($results as $item) {
				$user = User::find($item->user_id);
				$item->username = $user->name;
			}

			return view('search',compact('query','results'));
		}

	}

	public function feed() {
		
	    $feed = App::make("feed");

	    $feed->setCache(60, 'laravelFeedKey');

		   
	    if (!$feed->isCached())
	    {
	       // Добавляем в ленту последние 20 публикаций
	       $posts = \DB::table('posts')->orderBy('created_at', 'desc')->take(20)->get();

	       // Информация о ленте
	       $feed->title = 'Blog';
	       $feed->description = 'Blog Description';
	       $feed->link = url('rss');
	       $feed->setDateFormat('datetime');
	       $feed->pubdate = $posts[0]->created_at;
	       $feed->lang = 'ru';
	       $feed->setShortening(true);
	       $feed->setTextLimit(100);

	       foreach ($posts as $post)
	       {
	       	   // Получаем автора публикации
	       	   $user = User::find($post->user_id);

	           // set item's title, author, url, pubdate, description, content, enclosure (optional)*
	           $feed->add($post->title, $user->name, route('users.showPost',['name'=>$user->name,'id'=>$post->id]), $post->created_at, "", $post->text);
	       }

	    }

	    return $feed->render('atom');
	}

	public function bloggers() {

		//Массив со списком всех пользователей
		$bloggers = array();

		// Получаем список всех пользователей
		$users = DB::table('users')->get();
		foreach ($users as $user) {
			// Массив с данными по конкретному пользователю
			$userArray = array();
			$userArray['name'] = $user->name;

			// Получаем все публикации пользователя
			$posts = DB::table('posts')->where('user_id',$user->id)->get();

			$likes = 0;
			$hits = 0;

			// Проходимся по каждой публикации и 
			// получаем количество лайков и просмотров
			foreach ($posts as $post) {
				$likes += DB::table('likes')->where('post_id',$post->id)->count();
				$hits += $post->hits;
			}

			$userArray['likes'] = $likes;
			$userArray['hits'] = $hits;

			$bloggers[] = $userArray;
		}

		// 

		return view('bloggers',compact('bloggers'));
	}
}