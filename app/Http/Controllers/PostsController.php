<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Post;

class PostsController extends Controller 
{	
	public function show($id) {
		$post = Post::find($id);
		return view('post',compact('post'));
	}
}