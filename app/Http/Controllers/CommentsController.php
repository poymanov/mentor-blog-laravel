<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller 
{	
	public function postComment(Request $request) {

		$this->validate($request,[
			'comment_text' => "required|min:10",
			'g-recaptcha-response' => 'required|recaptcha'
		]);
	
		$post = new Comment();
		$post->comment_text = $request['comment_text'];
		$post->user_id = Auth::user()->id;
		$post->post_id = $request['post_id'];
		$post->save();

		return redirect()->route('post_show',['id'=>$request['post_id']]);
	}

	public function deleteComment($id) {
		$comment = Comment::find($id);
		
		if ($comment->delete()) {
			$message = "Comment successefully deleted!";
		}

		return redirect()->route('admin.comments')->with(['message' => $message]);
	}

	public function activateComment($id) {
		$comment = Comment::find($id);
		$comment->active = 1;

		if ($comment->update()) {
			$message = "Comment successefully activated!";
		}

		return redirect()->route('admin.comments')->with(['message' => $message]);
	}
}