<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Post;
use App\Comment;
use App\Like;
use DB;
use Mail;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller 
{	
	public function index($username) {

		// Проверяем, есть ли запрошенный пользователь в системе

		$user = User::select('id')->where('name',$username)->get();
		
		// Если такой пользователь найден
		// показываем его страницу
		if(count($user) > 0) {

			//Находим id пользователю по его name
			$user_id = $user[0]->id;
			$header = "Публикации автора @".$username;

			//Получаем все публикации пользователя
			$posts = Post::where('user_id',$user_id)
					->orderBy('created_at','desc')
					->where('status','1')
					->paginate(5);

			return view('users.index',compact('header','posts','username'));
		} else {
			// Если страницу смотрит авторизованный пользователь
			// Редиректим его на собственную страницу
			if (Auth::user()) {
				return $this->redirectUserPage();	
			} else {
				// Неавторизованного пользователя приглашем зарегистрироваться
				return view('auth.register');
			}
			
		}
	}

	public function newPost() {
		return view('users.newpost');
	}

	public function profile() {
		$user = Auth::user();
		return view('users.profile',compact('user'));	
	}

	public function comments() {
		$user = Auth::user();
		$comments = $user->commentsDesc;
		return view('users.comments',compact('user','comments'));	
	}

	public function likes() {
		$user = Auth::user();
		$likes = $user->likesDesc;
		return view('users.likes',compact('user','likes'));	
	}

	public function posts() {
		$user = Auth::user();
		$posts = $user->postsUpdated;

		foreach ($posts as $post) {
			// Добавляем информацию о количестве комментариев
			$comments = $post->comments;
			$post->comments_active = $comments->where('active','1')->count();
			$post->comments_moderate = $comments->where('active','0')->count();
		}

		return view('users.posts',compact('user','posts'));	
	}


	public function createPost(Request $request) {
		// Валидация
		$this->validatePost($request);

		// Запись в БД
		$post = new Post;
		$post->title = $request['title'];
		$post->text = $request['text'];
		$post->user_id = Auth::user()->id;
		$post->save();

		// Переадресация на главную страницу пользователя
		return $this->redirectUserPage();
	}

	public function editPost($id) {
		// Находим публикацию по id для редактирования

		$post = Post::find($id);
		return view('users.editpost',compact('post'));
	}

	public function updatePost(Request $request) {
		// Валидация
		$this->validatePost($request);

		// Находим публикацию по id и изменяем её
		$post = Post::find($request['post_id']);
		$post->title = $request['title'];
		$post->text = $request['text'];
		$post->update();

		// Редирект на страницу с управления записями пользователя
		return redirect()->route('users.posts');
	}

	public function deletePost($id) {
		// Находим публикацию по id и удаляем её
		$post = Post::find($id);
		$post->status = 0;
		$post->update();

		// Редирект на страницу пользователя
		return $this->redirectUserPage();
	}

	public function draftPost($id) {
		// Находим публикацию по id и переносим в черновики
		$post = Post::find($id);
		$post->status = 2;
		$post->update();

		// Редирект на страницу с управления записями пользователя
		return redirect()->route('users.posts');
	}

	public function undraftPost($id) {
		// Находим публикацию по id и переносим в опубликованные
		$post = Post::find($id);
		$post->status = 1;
		$post->update();

		// Редирект на страницу с управления записями пользователя
		return redirect()->route('users.posts');
	}

	public function showPost($name,$id) {

		$post = Post::find($id);

		// Если такой публикации нет вообще, то возвращаем 404
		if(!$post) {
			return response()->view('errors.404', [], 404);	
		}

		// Если публикация в черновиках
		// Или если её смотрит не её автор
		// Возвращаем 404

		if(Post::POST_DRAFT == $post->status && Auth::user() != $post->user) {
			return response()->view('errors.404', [], 404);	
		}

		// Если публикация удалена, то возвращем 410
		if(Post::POST_DELETE == $post->status) {
			return response()->view('errors.410', [], 410);	
		}			
		
		// Записываем просмотры публикации
		$post->hits++;
		$post->update();

		// Определяем, является ли текущий пользователь
		// автором публикации

		if (Auth::user() && Auth::user()->id == $post->user->id) 
		{
			$currentUser = true;
		}
		else
		{
			$currentUser = false;
		}

		// 3 последние записи из блога
		$lastPosts = Post::where('user_id',$post->user->id)
					 ->orderBy('created_at','desc')
					 ->take(3)->get();

		// 3 случаных записи из блога
		$randomPosts = Post::where('user_id',$post->user->id)
					  ->orderByRaw('RAND()')
					  ->take(3)
					  ->get();

		return view('posts.post',compact('post','currentUser','lastPosts','randomPosts'));
	}

	public function postComment(Request $request) {
		$this->validate($request,[
			'comment_text' => "required|min:10",
			'g-recaptcha-response' => 'required|recaptcha'
		]);
		
		$post_id = $request['post_id'];
		$user_id = Auth::user()->id;

		$comment = new Comment;
		$comment->comment_text = $request['comment_text'];
		$comment->user_id = $user_id;
		$comment->post_id = $post_id;

		// Получаем автора публикации
		$post = Post::find($post_id);
		$author = $post->user;

		// Если комментарий к собственному сообщению
		// Он публикуется автоматически
		// Комментарий к чужой публикации требует
		// Активации автора

		if ($user_id == $author->id) {
			$comment->active = Comment::COMMENT_PUBLISHED;			
		}

		$comment->save();

		if ($user_id == $author->id) {
			// Оповещение о новом комментарии
			$this->notifyNewCommentPost($post,$comment);
		}

		// Редирект на страницу публикации
		return redirect()->back();
	}

	public function deleteComment($id) {
		$comment = Comment::find($id);
		$comment->delete();
		return redirect()->back();
	}

	public function activateComment($id) {		
		$comment = Comment::find($id);
		$comment->active = Comment::COMMENT_PUBLISHED;
		$comment->update();

		// Получаем публикацию, к которой активируется комментарий
		$post = Post::find($comment->post_id);

		// Оповещение о новом комментарии
		$this->notifyNewCommentPost($post,$comment);

		return redirect()->back();
	}

	public function controlLike($id) {
		// Если лайк от текущего пользователя
		// для данной публикации уже есть
		// удаляем его, иначе устанавливаем

		// Проверяем существование публикации,
		// для которой хотим поставить лайк

		$post = Post::find($id);

		// Если такой публикации нет вообще, то возвращаем 404
		if(!$post) {
			return response()->view('errors.404', [], 404);	
		}

		// Если публикация в черновиках, 
		// Возвращаем 404

		if(Post::POST_DRAFT == $post->status) {
			return response()->view('errors.404', [], 404);	
		}

		// Если публикация удалена, то возвращем 410
		if(Post::POST_DELETE == $post->status) {
			return response()->view('errors.410', [], 410);	
		}

		$user_id = Auth::user()->id;

		$likes = DB::table('likes')
				 ->where('post_id',$id)
				 ->where('user_id',$user_id)
				 ->get();
		
		if(count($likes) > 0) {
			foreach ($likes as $item) {
				$like = Like::find($item->id);
				$like->delete();
			}
		} else {
			$like = new Like;
			$like->post_id = $id;
			$like->user_id = $user_id;
			$like->save();
		}
		
		return redirect()->back();
	}

	public function moderatecomments() {

		// Собираем все неактивные комментарии к 
		// публикациям текущего пользователя

		$user_id = Auth::user()->id;

		$comments = DB::table('comments')
						->where('comments.active',"0")
						->join('posts', 'comments.post_id','=','posts.id')
						->where('posts.user_id',$user_id)
						->select('comments.user_id','comments.comment_text','comments.id','posts.id as post_id','posts.title as post_title')
						->get();
		
		// Для каждого комментария получаем имя пользователя
		foreach ($comments as $item) {
			$item->username = User::find($item->user_id)->name;
		}

		return view('users.moderatecomments',compact('comments'));
	}

	// Общие процедуры 

	// Валидация формы публикации
	protected function validatePost($request) {
		$this->validate($request,[
			'title' => "required|min:5",
			'text' => 'required|min:5'
		]);
	}

	// Редирект на текущую страницу пользователя
	protected function redirectUserPage() {
		return redirect()->route('users.index',['name'=>Auth::user()->name]);
	}

	// Отправка email-сообщения всем кому понравилась
	// Публикация или кто оставил к ней комментарий
	protected function notifyNewCommentPost($post,$comment) {

		// id публикации
		$post_id = $post->id;
		
		// Массив пользователей получателей
		$users = array();

		// Все кому понравилась данная публикация и кто оставлял к ней комментарий
		$comments_table = DB::table('comments')
				  ->join('users','users.id','=','comments.user_id')
				  ->where('post_id',$post_id)
				  ->select('comments.user_id', 'users.name', 'users.email');

		$results = DB::table('likes')
				 ->join('users','users.id','=','likes.user_id')
				 ->where('post_id',$post_id)
				 ->union($comments_table)
				 ->select('user_id')
				 ->select('likes.user_id', 'users.name as username', 'users.email')
				 ->get();

		// Получаем имя автора публикации для передачи в текст сообщения
		$post_title = $post->title;
		$post_user = User::find($post->user_id);
		$post_username = $post_user->name;
		$post_user_email = $post_user->email;

		// Получаем автора комментария
		$comment_user = User::find($comment->user_id);
		$comment_username = $comment_user->name;

		// Получаем текст комментария
		$comment_text = $comment->comment_text;

		// Собираем пользователей и отправляем им сообщения

		foreach ($results as $item) {
			$users[$item->username] = $item->email;
		}

		// Удаляем из списка получателей автора комментария
		unset($users[$comment_username]);

		// Добавляем в список получателей автора публикации
		$users[$post_username] = $post_user_email;

		// Отправляем почтовое удомление всем пользователям

		foreach ($users as $name => $email) {
			$data['email'] = $email;
			$data['name'] = $name;
			$data['post_id'] = $post_id;
			$data['post_username'] = $post_username;
			$data['post_title'] = $post_title;
			$data['comment'] = $comment_text;

			Mail::queue('emails.notifycomment', ['data'=>$data], function ($m) use ($data) {
				$m->from(env('MAIL_FROM'), env('MAIL_SUBJ'));
				$m->to($data['email'], $data['name'])->subject('Новый комментарий к интересующей вас публикации');
			});
		}
	}

}