<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Post;
use App\Comment;

class AdminController extends Controller 
{	
	public function index() {
		if (Auth::user()->is_admin == 1) {
			return view('admin.dashboard');	
		} else {
			return redirect()->route('index');
		}
	}

	public function posts() {
		if (Auth::user()->is_admin == 1) {
			$posts = Post::orderBy('created_at','desc')->get();
			return view('admin.posts',compact('posts'));
		} else {
			return redirect()->route('index');
		}
	}

	public function newPost() {
		return view('admin.new_post');
	}

	public function editPost($id) {
		$post = Post::find($id);
		return view('admin.edit_post',compact('post'));
	}

	public function updatePost(Request $request) {
		$this->validate($request,[
			'title' => "required|min:5",
			'preview_text' => "required|min:140",
			'full_text' => "required|min:140"
		]);

		$post = Post::find($request['id']);
		$post->title = $request['title'];
		$post->preview_text = $request['preview_text'];
		$post->full_text = $request['full_text'];

		if ($post->update()) {
			$message = "Post successefully updated!";
		}

		return redirect()->route('admin.posts')->with(['message' => $message]);
	}

	public function createPost(Request $request) {
		$this->validate($request,[
			'title' => "required|min:5",
			'preview_text' => "required|min:140",
			'full_text' => "required|min:140"
		]);

		$post = new Post();
		$post->title = $request['title'];
		$post->preview_text = $request['preview_text'];
		$post->full_text = $request['full_text'];

		$message = "There was an error";

		if ($post->save()) {
			$message = "Post successefully created!";
		}

		return redirect()->route('admin.posts')->with(['message' => $message]);
	}

	public function deletePost($id){
		$post = Post::find($id);
	
		if ($post->delete()) {
			$message = "Post successefully deleted!";
		}

		return redirect()->route('admin.posts')->with(['message' => $message]);	
	}

	public function getComments() {
		if (Auth::user()->is_admin == 1) {
			$comments = Comment::orderBy('created_at','desc')->get();
			return view('admin.comments',compact('comments'));
		} else {
			return redirect()->route('index');
		}
	}
}