<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Comment;

class CheckCommentActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Проверяем что текущий пользователь активирует
        // комментарий в пределах своего блога

        // Получаем автора публикаций, к которой относится 
        // активируемая публикация
        $comment = Comment::find($request->id);

        // Если такого комментария нет, возврщаем 404
        if(!$comment) {
            return response()->view('errors.404', [], 404); 
        }

        $user_id = $comment->post->user_id;

        if(Auth::user()->id != $user_id) {
            return response()->view('errors.401', [], 401);
        }

        return $next($request);
    }
}
