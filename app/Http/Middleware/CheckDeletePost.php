<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Post;

class CheckDeletePost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Проверяем существование публикации
        $post = Post::find($request->id);

        // Если такой публикации нет вообще, то возвращаем 404
        if(!$post) {
            return response()->view('errors.404', [], 404); 
        }

        // Если публикация удалена, то возвращем 410
        if(Post::POST_DELETE == $post->status) {
            return response()->view('errors.410', [], 410); 
        }  
        
        // Проверяем, действительно ли пользователь 
        // удаляет принадлежащую ему публикацию

        // Получаем информацию о публикации
        $post = Post::find($request->id);

        if(Auth::user()->id != $post->user_id) {
            return response()->view('errors.401', [], 401);
        }

        return $next($request);
    }
}
