<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

		Route::get('/', [
			'uses' => 'MainController@index',
			'as' => 'index'
		]);

		Route::get('/search', [
			'uses' => 'MainController@searchPage',
			'as' => 'search'
		]);

		Route::post('/search/find', [
			'uses' => 'MainController@searchResults',
			'as' => 'find.posts'
		]);

		Route::get('/bloggers', [
			'uses' => 'MainController@bloggers',
			'as' => 'bloggers'
		]);

		// Роутинг для сущности users
		Route::group(['prefix' => 'users'], function () {
			// Роутинг для для профиля пользователя
			Route::group(['prefix' => 'profile'], function () {
				// Страница профиля пользователя
				Route::get('/',[
					'uses' => 'UsersController@profile',
					'as' => 'users.profile',
		    		'middleware' => 'auth'
				]);

				// Страница комментариев пользователя
				Route::get('/comments',[
					'uses' => 'UsersController@comments',
					'as' => 'users.comments',
		    		'middleware' => 'auth'
				]);

				// Страница понравившихся записей пользователя
				Route::get('/likes',[
					'uses' => 'UsersController@likes',
					'as' => 'users.likes',
		    		'middleware' => 'auth'
				]);

				// Страница управлениям записями пользователя
				Route::get('/posts',[
					'uses' => 'UsersController@posts',
					'as' => 'users.posts',
		    		'middleware' => 'auth'
				]);

				// Страница подтверждения комментариев из собственного блога
				Route::get('/validatecomments',[
					'uses' => 'UsersController@moderatecomments',
					'as' => 'users.moderatecomments',
		    		'middleware' => 'auth'
				]);
			});

			// Индексная страница пользователя
			// со списком собственных публикаций
			Route::get('/{name}',[
				'uses' => 'UsersController@index',
				'as' => 'users.index'
			]);

			// Страница добавления собственной публикации
			Route::get('/{name}/new',[
	    		'uses' => 'UsersController@newPost',
	    		'as' => 'users.newPost',
	    		'middleware' => 'auth'
    		]);

    		// Действие - создание публикации
			Route::post('/create',[
	    		'uses' => 'UsersController@createPost',
	    		'as' => 'users.createPost',
	    		'middleware' => 'auth'
    		]);

    		// Страница редактирования публикации
    		Route::get('/edit/{id}',[
	    		'uses' => 'UsersController@editPost',
	    		'as' => 'users.editPost',
	    		'middleware' => ['auth','check.post.edit']
    		]);

    		// Действие - обновление публикации
    		Route::post('/update/',[
	    		'uses' => 'UsersController@updatePost',
	    		'as' => 'users.updatePost',
	    		'middleware' => 'auth'
    		]);

    		// Действие - добавление публикации в черновик
    		Route::get('/draft/{id}',[
	    		'uses' => 'UsersController@draftPost',
	    		'as' => 'users.draftPost',
	    		'middleware' => ['auth','check.post.draft']
    		]);

   			// Действие - перенос публикации из черновиков в опубликованные
    		Route::get('/undraft/{id}',[
	    		'uses' => 'UsersController@undraftPost',
	    		'as' => 'users.undraftPost',
	    		'middleware' => ['auth','check.post.undraft']
    		]);

    		// Действие - удаление публикации
    		Route::get('/delete/{id}',[
	    		'uses' => 'UsersController@deletePost',
	    		'as' => 'users.deletePost',
	    		'middleware' => ['auth','check.post.delete']	
    		]);

    		// Страница полного текст публикации
			Route::get('/{name}/posts/{id}',[
	    		'uses' => 'UsersController@showPost',
	    		'as' => 'users.showPost'
    		]);

    		// Действие - создание комментария
    		Route::post('/comments/new',[
    			'uses' => 'UsersController@postComment',
	    		'as' => 'users.postComment',
	    		'middleware' => 'auth'
    		]);

    		// Действие - удаление комментария
    		Route::get('/comments/delete/{id}',[
    			'uses' => 'UsersController@deleteComment',
	    		'as' => 'users.deleteComment',
	    		'middleware' => ['auth','check.comment.delete']
    		]);

    		// Действие - активация комментария
    		Route::get('/comments/activate/{id}',[
    			'uses' => 'UsersController@activateComment',
	    		'as' => 'users.activateComment',
	    		'middleware' => ['auth','check.comment.active']
    		]);

    		// Действие - управление лайками
    		Route::get('/likes/{id}',[
    			'uses' => 'UsersController@controlLike',
	    		'as' => 'users.controlLike',
	    		'middleware' => 'auth'
    		]);
		});

		Route::get('/admin', [
			'uses' => 'AdminController@index',
			'as' => 'admin.index',
			'middleware' => 'auth'
		]);

		Route::get('/admin/posts', [
			'uses' => 'AdminController@posts',
			'as' => 'admin.posts',
			'middleware' => 'auth'
		]);

		Route::get('/admin/new', [
			'uses' => 'AdminController@newPost',
			'as' => 'admin.new.post',
			'middleware' => 'auth'
		]);

		Route::post('/admin/create', [
			'uses' => 'AdminController@createPost',
			'as' => 'admin.create.post',
			'middleware' => 'auth'
		]);

		Route::post('/admin/update', [
			'uses' => 'AdminController@updatePost',
			'as' => 'admin.update.post',
			'middleware' => 'auth'
		]);

		Route::get('/admin/edit/{id}', [
			'uses' => 'AdminController@editPost',
			'as' => 'admin.edit.post',
			'middleware' => 'auth'
		]);

		Route::get('/admin/delete/{id}', [
			'uses' => 'AdminController@deletePost',
			'as' => 'admin.del.post',
			'middleware' => 'auth'
		]);

		Route::get('/comments', [
			'uses' => 'AdminController@getComments',
			'as' => 'admin.comments'
		]);

		Route::get('/comments/delete/{id}', [
			'uses' => 'CommentsController@deleteComment',
			'as' => 'admin.delete.comments'
		]);

		Route::get('/comments/activate/{id}', [
			'uses' => 'CommentsController@activateComment',
			'as' => 'admin.activate.comment'
		]);

		Route::get('/rss', [
			'uses' => 'MainController@feed',
			'as' => 'feed'
		]);

		Route::get('/sitemap', [
			'uses' => 'MainController@sitemap',
			'as' => 'sitemap'
		]);

		Route::auth();
});
