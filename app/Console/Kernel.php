<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use App;
use App\User;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function(){
          // Статические страницы
          $sitemap_static = App::make("sitemap");
          $sitemap_static->add(url('/'), date('c'), '1.0', 'daily');
          $sitemap_static->add(route('bloggers'), date('c'), '1', 'daily');

          // Сохраняем sitemap со статическими страницами
          $sitemap_static->store('xml','sitemap-static');

          // Блоггеры
          $sitemap_bloggers = App::make("sitemap");

          $bloggers = DB::table('users')->orderBy('created_at', 'desc')->get();

          foreach ($bloggers as $blogger){
              $sitemap_bloggers->add(route('users.index',['name'=>$blogger->name]), date('c', strtotime($blogger->updated_at)), '1.0', 'daily');
          }

          // Сохраняем sitemap со списком блоггеров
          $sitemap_bloggers->store('xml','sitemap-bloggers');

          // Публикации
          $sitemap_posts = App::make("sitemap");

          $posts = DB::table('posts')->orderBy('created_at', 'desc')->get();

          foreach ($posts as $post){
            // Формируем адрес каждой публикации с учетом имени автора
            $user = User::find($post->user_id);
            $sitemap_posts->add(route('users.showPost',['name'=>$user->name,'id'=>$post->id]), $post->updated_at, '1', 'daily');
          }

          // Сохраняем sitemap со списком публикаций
          $sitemap_posts->store('xml','sitemap-posts');

          // Индексный файл
          $sitemap = App::make("sitemap");

          $sitemap->addSitemap(url('sitemap-static.xml'),date('c'));
          $sitemap->addSitemap(url('sitemap-bloggers.xml'),date('c'));
          $sitemap->addSitemap(url('sitemap-posts.xml'),date('c'));

          // Сохраняем индексный файл
          $sitemap->store('sitemapindex','sitemap');
        })->hourly();
    }

}
