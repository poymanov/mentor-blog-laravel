<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{	

	const COMMENT_PUBLISHED = 1;
	const COMMENT_UNPUBLISHED = 0;

    public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function post()
	{
		return $this->belongsTo('App\Post');
	}
}
