<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	const POST_DELETE = 0;
	const POST_PUBLISHED = 1;
	const POST_DRAFT = 2;


	public function comments()
	{
	    return $this->hasMany('App\Comment')->orderBy('created_at', 'desc');
	}

	public function likes()
	{
	    return $this->hasMany('App\Like');
	}

	public function user()
	{
	    return $this->belongsTo('App\User');
	}

	public function activeComments()
	{
	    return $this->hasMany('App\Comment')->where('active','1');
	}
}
