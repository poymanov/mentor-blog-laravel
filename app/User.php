<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function commentsDesc()
    {
        return $this->hasMany('App\Comment')->orderBy('created_at','desc');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function postsUpdated()
    {
        return $this->hasMany('App\Post')->orderBy('updated_at','desc');;
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function likesDesc()
    {
        return $this->hasMany('App\Like')->orderBy('created_at', 'desc');
    }

}


