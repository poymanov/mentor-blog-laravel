<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	public function run()
	{
		DB::table('users')->insert([
            'name' => 'demo',
            'email' => 'demo@demo.ru',
            'password' => bcrypt('demo')
        ]);
	}
}
