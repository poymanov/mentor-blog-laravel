var editor_config = {
	path_absolute: "{{ URL::to('/') }}/",
	selector: "textarea",
	relative_urls: false
}

tinymce.init(editor_config);