<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') - Коллективные блоги</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Николай Пойманов, github.com/poymanov">
	<link rel="stylesheet" href="{{ URL::to('assets/css/vendor.min.css') }}">
	<link rel="stylesheet" href="{{ URL::to('assets/css/main.min.css') }}">
	<link rel="alternate" type="application/rss+xml" title="Последние сообщения - Мультиблог платформа" href="{{route('feed')}}" />
</head>