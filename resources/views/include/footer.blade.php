<footer class="main-footer">
  <p>Copyrights (c) <a href="#">Fikristudio</a> 2016</p>
  <p>Портфолио-проект <a href="mailto:n.poymanov@gmail.com">Пойманова Николая</a></p>
  <p><a href="https://bitbucket.org/poymanov/mentor-blog-laravel" target="_blank">Исходный код</a></p>
</footer>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="{{ URL::to('assets/js/script.js') }}"></script>