<nav class="main-nav">
	<ul class="main-nav__list">
		<li class="main-nav__item">
			<a href="{{ route('index') }}" class="main-nav__link">Главная</a>
		</li>
		<li class="main-nav__item">
			<a href="{{ route('bloggers') }}" class="main-nav__link">Авторы</a>
		</li>
		@if (Auth::user())
			<li class="main-nav__item">
				<a href="{{ route('users.index',['name'=>Auth::user()->name]) }}" class="main-nav__link">Моя страница</a>
			</li>
			<li class="main-nav__item">
				<a href="{{ route('users.profile') }}" class="main-nav__link">Профиль</a>
			</li>
		@else
			<li class="main-nav__item">
				<a href="/login" class="main-nav__link">Войти</a>
			</li>
			<li class="main-nav__item">
				<a href="/register" class="main-nav__link">Регистрация</a>
			</li>
		@endif
		<li class="main-nav__item">
			<a href="{{ route('search') }}" class="main-nav__link">Поиск</a>
		</li>
		<li class="main-nav__item">
			<a href="{{ route('feed') }}" class="main-nav__link">RSS</a>
		</li>
	</ul>
</nav>