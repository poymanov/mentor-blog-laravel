@extends('layouts.admin')
@section('title',$post->title." - Admin")
@section('content')

<div class="col-md-10">
	<div class="content-box-large">
		<div class="panel-body">
			<div class="panel-title">
				<h1>{{ $post->title }} - Edit post</h1>
			</div>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" role="form" method="POST" action="{{ route('admin.update.post') }}">
				{!! csrf_field() !!}
				<input type="hidden" name="id" value="{{ $post->id }}">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-1 control-label">Post title</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="inputEmail3" name="title" value="{{ $post->title }}">
							@if ($errors->has('title'))
								<div class="has-error">
									<span class="help-block">{{ $errors->first('title') }}</span>	
								</div>
							@endif
					</div>			    
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">Preview text</label>
					<div class="col-sm-4">
						<textarea class="form-control" rows="3" name="preview_text">{{ $post->preview_text }}</textarea>
							@if ($errors->has('preview_text'))
								<div class="has-error">
									<span class="help-block">{{ $errors->first('preview_text') }}</span>	
								</div>
							@endif
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">Full text</label>
					<div class="col-sm-4">
						<textarea class="form-control" rows="3" name="full_text">{{ $post->full_text }}</textarea>
							@if ($errors->has('full_text'))
								<div class="has-error">
									<span class="help-block">{{ $errors->first('full_text') }}</span>	
								</div>
							@endif
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-1 col-sm-1">
						<button type="submit" class="btn btn-primary">Update</button>						
					</div>
					<div class="col-sm-1">
						<a href="{{ route('admin.del.post',['id' => $post->id]) }}" class="btn btn-danger">Delete</a>
					</div>					
				</div>
		</form>
		</div>
	</div>
</div>

@endsection