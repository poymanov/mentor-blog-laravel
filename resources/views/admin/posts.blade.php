@extends('layouts.admin')
@section('title',"Posts - Admin")
@section('content')
<div class="col-md-10">
	<div class="content-box-large">
		@include('admin.message')
		<div class="panel-heading">
			<div class="panel-title">Blog Posts</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<a href="{{ route('admin.new.post') }}" class="btn btn-primary">New post</a>
				<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Created at</th>
								<th>Updated at</th>
								<th>Comments</th>
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $index => $post)
								<tr>
									<td>{{ $index + 1 }}</td>
									<td>
										<a href="{{ route('admin.edit.post',['id' => $post->id]) }}">
											{{ $post->title }}
										</a>
									</td>
									<td>{{ $post->created_at }}</td>
									<td>{{ $post->updated_at }}</td>
									<td>111</td>
								</tr>
							@endforeach
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>
@endsection