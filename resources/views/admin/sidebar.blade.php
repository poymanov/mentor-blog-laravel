<div class="col-md-2">
	<div class="sidebar content-box" style="display: block;">
		<ul class="nav">
				<li class="current">
					<a href="{{ route('admin.index') }}">
						<i class="glyphicon glyphicon-home"></i> Dashboard
					</a>
				</li>				
				<li>
					<a href="{{ route('admin.posts') }}">
						<i class="glyphicon glyphicon-list"></i>Posts
					</a>
				</li>
				<li>
					<a href="{{ route('admin.comments') }}">
						<i class="glyphicon glyphicon-pencil"></i>Comments
					</a>
				</li>
		</ul>
 </div>
</div>