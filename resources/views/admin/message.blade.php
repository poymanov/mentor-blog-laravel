@if (Session::has('message'))
	<div class="row">
		<div class="col-md-4 col-md-offset-4 success">
			<h2>
				{{ Session::get('message') }}	
			</h2>			
		</div>
	</div>
@endif