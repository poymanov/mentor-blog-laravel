<head>
  <title>@yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{ URL::to('assets/admin/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::to('assets/admin/css/styles.css') }}">
</head>