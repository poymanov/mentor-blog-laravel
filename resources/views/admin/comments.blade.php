@extends('layouts.admin')
@section('title',"Comments - Admin")
@section('content')
<div class="col-md-10">
	<div class="content-box-large">
		@include('admin.message')
		<div class="panel-heading">
			<div class="panel-title">Blog Comments</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">				
				<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Text</th>
								<th>Author</th>
								<th>Created at</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($comments as $index => $comment)
								<tr>
									<td>{{ $index + 1 }}</td>
									<td>
										{{ $comment->comment_text }}
									</td>
									<td>
										{{ $comment->user->name }}
									</td>
									<td>
										{{ $comment->created_at }}
									</td>
									<td>
										@if ($comment->active == 0)
											<a class="btn btn-warning" href="{{ route('admin.activate.comment',['id' => $comment->id]) }}">Activate</a>										
										@endif
									</td>
									<td>
										<a class="btn btn-danger" href="{{ route('admin.delete.comments',['id' => $comment->id]) }}">Delete</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>
@endsection