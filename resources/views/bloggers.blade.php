@extends('layouts.master')
@section('title',"Авторы")
@section('content')
<div class="content-layout">
	<div class="auth">
		<h1 class="header-content">Авторы коллективного блога</h1>
		<table class="posts__table">
			<tr>
				<th>#</th>
				<th class="bloggers__table-th-width">Автор</th>
				<th>Оценили записи</th>
				<th>Просмотры</th>
			</tr>
			@foreach($bloggers as $index=>$blogger)
				<tr>
					<td>{{$index + 1}}</td>
					<td>
						<a href="{{ route('users.index',['name'=>$blogger['name']]) }}" class="posts__table__link">{{ $blogger['name'] }}</a>
					</td>
					<td>{{ $blogger['likes'] }}</td>
					<td>{{ $blogger['hits'] }}</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection