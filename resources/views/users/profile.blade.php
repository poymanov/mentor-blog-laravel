@extends('layouts.master')
@section('title',"Профиль @".$user->name)
@section('content')
<div class="content-layout">
  <div class="heading">
    <h1>Профиль пользователя {{"@".$user->name}}</h1>
  </div>
  <div class="profile-content">
    <ul class="profile__list">
      <li class="profile__item">
      	<a href="{{ route('users.newPost',['name'=>$user->name]) }}" class="profile__link">Новая публикация</a>
      </li>
      <li class="profile__item">
      	<a href="{{ route('users.posts') }}" class="profile__link">Управление публикациями</a>
      </li>
      <li class="profile__item">
      	<a href="{{ route('users.comments') }}" class="profile__link">Мои комментарии</a>
      </li>
      <li class="profile__item">
        <a href="{{ route('users.moderatecomments') }}" class="profile__link">Управление комментариями</a>
      </li>
      <li class="profile__item">
      	<a href="{{ route('users.likes') }}" class="profile__link">Мои лайки</a>
      </li>
      <li class="profile__item">
      	<a href="/logout" class="profile__link">Выход</a>
      </li>
    </ul>
  </div>
</div>
@endsection