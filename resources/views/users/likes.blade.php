@extends('layouts.master')
@section('title',"Понравившиеся @".$user->name)
@section('content')
<div class="content-layout">
  <div class="auth">
    <h1 class="header-content">Понравившиеся {{ "@".$user->name }}</h1>
    <ul class="result__list">
      @foreach($likes as $like)
        <li class="result__item">
          <a href="{{ route('users.showPost',['name'=>$like->post->user->name,'id'=>$like->post->id]) }}" class="result__header">{{ $like->post->title }}</a>
          <a href="{{ route('users.index',['name'=>$like->post->user->name]) }}" class="result__author">{{ $like->post->user->name }}</a>
          <p class="result__text">{{ $like->post->text }}</p>
        </li>
      @endforeach
    </ul>
  </div>
</div>
@endsection