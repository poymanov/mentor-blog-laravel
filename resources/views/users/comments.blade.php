@extends('layouts.master')
@section('title',"Комментарии @".$user->name)
@section('content')
<div class="content-layout">
  <div class="auth">
    <h1 class="header-content">Комментарии {{ "@".$user->name }}</h1>
    <ul class="result__list">
      @foreach($comments as $comment)
        <li class="result__item">
          <a href="{{ route('users.showPost',['name'=>$comment->post->user->name,'id'=>$comment->post->id]) }}" class="result__header">
            {{ $comment->post->title }} ({{ "@".$comment->post->user->name }} - {{ $comment->post->created_at }})
          </a>        
          <p class="result__text">
            <strong>{{"@".$comment->user->name }} ({{ $comment->created_at }}):</strong> {{ $comment->comment_text }}</p>
        </li>
      @endforeach
    </ul>
  </div>
</div>
@endsection