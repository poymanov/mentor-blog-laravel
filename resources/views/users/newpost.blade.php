@extends('layouts.master')
@section('title',"Главная")
@section('content')
<div class="content-layout">
	<div class="blog-content">
		<div class="new-post">
			<h1 class="new-post__header">Новая публикация</h1>
			<form action="{{route('users.createPost')}}" class="new-comment__form" method="POST">
				{!! csrf_field() !!}
				<div class="new-post__left">
					<div class="new-post__wrapper">
						<label for="title" class="new-post__label">Заголовок</label>
						<input id="title" type="text" value="{{ old('title') }}" name="title" class="new-post__input">
						@if ($errors->has('title'))
							<div class="form__error">{{$errors->first('title')}}</div>
						@endif
					</div>
					<div class="new-post__wrapper">
						<label for="text" class="new-post__label">Сообщение</label>
						<textarea id="text" name="text" class="new-post__textarea">{{ old('text') }}</textarea>
						@if ($errors->has('text'))
							<div class="form__error">{{$errors->first('text')}}</div>
						@endif
					</div>
				</div>
				<div class="new-post__right">
					<div class="new-post__wrapper">
						<input type="submit" value="Опубликовать" class="new-post__button new-post__button-post">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ URL::to('vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ URL::to('assets/js/tinymce.js') }}"></script>
@endsection

