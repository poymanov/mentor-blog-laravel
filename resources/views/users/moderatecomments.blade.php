@extends('layouts.master')
@section('title',"Управление комментариями")
@section('content')
<div class="content-layout">
	<div class="auth">
		<h1 class="header-content">Управление комментариями</h1>
		<ul class="moderatecomments__list">
			@if (count($comments) > 0)
				@foreach($comments as $comment)
					<li class="moderatecomments__item">
						<div class="moderatecomments__header">
							<a href="{{ route('users.showPost',['name'=>$comment->username,'id'=>$comment->post_id]) }}" class="moderatecomments__header-link">{{ $comment->post_title }}</a>
						</div>
						<div class="moderatecomments__content">
							<p class="moderatecomments__content-text">
								<a href="{{ route('users.index',['name'=>$comment->username]) }}" class="moderatecomments__content-user">{{ $comment->username }}</a>: {{$comment->comment_text }}
							</p>
						</div>
						<div class="moderatecomments__footer">
							<a href="{{ route('users.activateComment',['id'=>$comment->id])}}" class="moderatecomments__footer-link moderatecomments__footer-link-resolve">Опубликовать</a><a href="{{route('users.deleteComment',['id'=>$comment->id])}}" class="moderatecomments__footer-link moderatecomments__footer-link-reject">Удалить</a></div>
					</li>
				@endforeach
			@else
				Нет комментариев для модерации
			@endif
		</ul>
	</div>
</div>
@endsection