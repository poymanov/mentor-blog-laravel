@extends('layouts.master')
@section('title',$post->title)
@section('content')
<div class="content-layout">
	<div class="blog-content">
		<div class="new-post">
			<h1 class="new-comment__header">Редактирование публикации</h1>
			<form action="{{ route('users.updatePost') }}" class="new-comment__form" method="POST">
				{!! csrf_field() !!}
				<div class="new-post__left">
					<input type="hidden" name="post_id" value="{{ $post->id }}">
					<div class="new-post__wrapper">
						<label for="title" class="new-post__label">Заголовок</label>
						<input id="title" type="text" name="title" value="{{ $post->title }}" class="new-post__input">
						@if ($errors->has('title'))
							<div class="form__error">{{$errors->first('title')}}</div>
						@endif
					</div>
					<div class="new-post__wrapper">
						<label for="text" class="new-post__label">Сообщение</label>
						<textarea id="text" name="text" class="new-post__textarea">{{ $post->text }}</textarea>
						@if ($errors->has('text'))
							<div class="form__error">{{$errors->first('text')}}</div>
						@endif
					</div>
				</div>
				<div class="new-post__right">
					<div class="new-post__wrapper">
						@if ($post->status == 1)
							<input type="submit" value="Сохранить" class="new-post__button new-post__button-post">
							<a href="{{ route('users.deletePost',['id'=>$post->id]) }}" class="new-post__button new-post__button-delete">Удалить</a>
							<a href="{{ route('users.draftPost',['id'=>$post->id]) }}" class="new-post__button new-post__button-draft">В черновик</a>
						@else
							<a href="{{ route('users.undraftPost',['id'=>$post->id]) }}" class="new-post__button new-post__button-post">Опубликовать</a>
							<a href="{{ route('users.deletePost',['id'=>$post->id]) }}" class="new-post__button new-post__button-delete">Удалить</a>						
						@endif
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ URL::to('vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ URL::to('assets/js/tinymce.js') }}"></script>
@endsection