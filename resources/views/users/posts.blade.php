@extends('layouts.master')
@section('title',"Управление публикациями @".$user->name)
@section('content')
<div class="content-layout">
  <div class="auth">
    <h1 class="header-content">Управление публикациями {{ "@".$user->name }}</h1>
    <table class="posts__table">
      <tr>
        <th>#</th>
        <th class="header-content__th_width">Название</th>
        <th>Лайки</th>
        <th>Просмотры</th>
        <th title="Опубликованных/на модерации">Комменты</th>      
        <th>Создано</th>
        <th>Изменено</th>
        <th class="header-content__th_width2">Статус</th>
      </tr>
      @foreach($posts as $index=>$post)
        <tr>
          <td>{{ $index + 1 }}</td>
          <td>
            @if($post->status == 0)
              {{ $post->title }}
            @else
              <a href="{{ route('users.editPost',['id'=>$post->id]) }}" class="posts__table__link">{{ $post->title }}</a>
              <a href="{{route('users.showPost',['name'=>$post->user->name,'id'=>$post->id])}}" class="posts__table__link" target="_blank">(как выглядит?)</a>
            @endif
          </td>
          <td>{{ $post->likes->count() }}</td>
          <td>{{ $post->hits }}</td>
          <td>{{ $post->comments_active }} | <span class="posts__table__moderate">{{ $post->comments_moderate }}</span></td>
          <td>{{ $post->created_at }}</td>
          <td>{{ $post->updated_at }}</td>
          <td>
            @if($post->status == 1)
              Опубликовано
            @elseif($post->status == 2)
              Черновик
            @elseif($post->status == 0)
              Удалено
            @endif
          </td>
        </tr>
      @endforeach
    </table>
  </div>
</div>
@endsection