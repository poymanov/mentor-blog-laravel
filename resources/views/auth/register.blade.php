@extends('layouts.master')
@section('title',"Регистрация")
@section('content')

<div class="content-layout">
  <div class="auth">
    <h1 class="header-content">Регистрация</h1>
    <form class="form__auth" role="form" method="POST" action="{{ url('/register') }}">
    	{!! csrf_field() !!}
      <div class="form__wrapper">
	      <label for="email" class="form__label">E-mail:</label>
	      <input type="email" id="email" name="email" class="form__input" value="{{ old('email') }}">
	      @if ($errors->has('email'))
	      	<div class="form__error">{{ $errors->first('email') }}</div>
	      @endif
      </div>
      <div class="form__wrapper">
        <label for="name" class="form__label">имя:</label>
        <div class="form__input-name">
        	<input type="text" id="name" name="name" value="{{ old('name') }}" class="form__input">
        </div>
      	@if ($errors->has('name'))
        	<div class="form__error">{{ $errors->first('name') }}</div>
        @endif
      </div>
      <div class="form__wrapper">
      	<label for="password" class="form__label">Пароль:</label>
      	<input type="password" id="password" name="password" class="form__input">
      	@if ($errors->has('password'))
        	<div class="form__error">{{ $errors->first('password') }}</div>
        @endif
      </div>
      <div class="form__wrapper">
      	<label for="password-repeat" class="form__label">Повтор пароля:</label>
      	<input type="password" id="password-repeat" name="password_confirmation" class="form__input">
      	@if ($errors->has('password_confirmation'))
        	<div class="form__error">{{ $errors->first('password_confirmation') }}</div>
        @endif
      </div>
      <div class="form__wrapper">
        <input type="submit" value="Отправить" class="form__button">
      </div>
    </form>
  </div>
</div>
@endsection
