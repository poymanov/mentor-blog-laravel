@extends('layouts.app')

@section('content')

<div class="content-layout">
	<div class="auth">
		<h1 class="header-content">Reset Password</h1>
		<form class="form__auth">
			<div class="form__wrapper"><label for="email" class="form__label">E-mail:</label><input type="email" id="email" name="email" class="form__input">
				<div class="form__error">Error message</div>
			</div>
			<div class="form__wrapper"><input type="submit" value="Send Password Reset Link" class="form__button"></div>
		</form>
	</div>
</div>









<div class="container">
		<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="panel panel-default">
								<div class="panel-heading">Reset Password</div>

								<div class="panel-body">
										<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
												{!! csrf_field() !!}

												<input type="hidden" name="token" value="{{ $token }}">

												<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
														<label class="col-md-4 control-label">E-Mail Address</label>

														<div class="col-md-6">
																<input type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

																@if ($errors->has('email'))
																		<span class="help-block">
																				<strong>{{ $errors->first('email') }}</strong>
																		</span>
																@endif
														</div>
												</div>

												<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
														<label class="col-md-4 control-label">Password</label>

														<div class="col-md-6">
																<input type="password" class="form-control" name="password">

																@if ($errors->has('password'))
																		<span class="help-block">
																				<strong>{{ $errors->first('password') }}</strong>
																		</span>
																@endif
														</div>
												</div>

												<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
														<label class="col-md-4 control-label">Confirm Password</label>
														<div class="col-md-6">
																<input type="password" class="form-control" name="password_confirmation">

																@if ($errors->has('password_confirmation'))
																		<span class="help-block">
																				<strong>{{ $errors->first('password_confirmation') }}</strong>
																		</span>
																@endif
														</div>
												</div>

												<div class="form-group">
														<div class="col-md-6 col-md-offset-4">
																<button type="submit" class="btn btn-primary">
																		<i class="fa fa-btn fa-refresh"></i>Reset Password
																</button>
														</div>
												</div>
										</form>
								</div>
						</div>
				</div>
		</div>
</div>
@endsection
