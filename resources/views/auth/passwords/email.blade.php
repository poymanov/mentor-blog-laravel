@extends('layouts.master')

<!-- Main Content -->
@section('content')

<div class="content-layout">
	<div class="auth">
		<h1 class="header-content">Reset Password</h1>
		<form class="form__auth" method="POST" action="{{ url('/password/email') }}">
			{!! csrf_field() !!}
			@if (session('status'))
				<div class="alert alert-success">
						{{ session('status') }}
				</div>
			@endif
			<div class="form__wrapper">
				<label for="email" class="form__label">E-mail:</label>
				<input type="email" id="email" name="email" value="{{ old('email') }}" class="form__input">
				@if ($errors->has('email'))
					<div class="form__error">{{ $errors->first('email') }}</div>
				@endif
			</div>
			<div class="form__wrapper"><input type="submit" value="Send Password Reset Link" class="form__button"></div>
		</form>
	</div>
</div>
@endsection
