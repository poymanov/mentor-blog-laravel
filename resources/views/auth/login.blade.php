@extends('layouts.master')
@section('title',"Войти")
@section('content')

<div class="content-layout">
	<div class="auth">
		<h1 class="header-content">Войти</h1>
		<form class="form__auth" method="POST" action="{{ url('/login') }}">
			{!! csrf_field() !!}
			<div class="form__wrapper">
				<label for="email" class="form__label">E-mail:</label>
				<input type="email" id="email" name="email" class="form__input" value="{{ old('email') }}">
				@if ($errors->has('email'))
					<div class="form__error">{{ $errors->first('email') }}</div>
				@endif
			</div>
			<div class="form__wrapper">
				<label for="password" class="form__label">Пароль:</label>
				<input type="password" id="password" name="password" class="form__input">
				@if ($errors->has('password'))
					<div class="form__error">{{ $errors->first('password') }}</div>
				@endif
			</div>
			<div class="form__wrapper">
				<label for="remember" class="form__label">
				<input type="checkbox" name="remember" id="remember" class="form__checkbox">Запомнить меня</label>
			</div>
			<div class="form__wrapper">
				<input type="submit" value="Войти" class="form__button">
			</div>
			<a href="{{ url('/password/reset') }}" class="form__link">Забыли пароль?</a>
		</form>
	</div>
</div>
@endsection