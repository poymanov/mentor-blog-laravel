<div class="posts__info">
	<ul class="posts__info-list">
		<li class="posts__info-item">
			<span class="posts__info-text">Автор: </span>
			<a href="{{ route('users.index',['name' => $post->user->name]) }}" class="posts__info-link_type1">{{ "@".$post->user->name }}</a>
		</li>
		<li class="posts__info-item">
			<a href="{{ route('users.controlLike',['id'=>$post->id]) }}" class="posts__info-link posts__info-link-likes">{{ $post->likes->count() }}</a>
		</li>
		<li class="posts__info-item">
			<a href="#comments" class="posts__info-link posts__info-link-comments">{{ $post->activeComments->count() }}</a>
		</li>
		<li class="posts__info-item">
			<span class="posts__info-text">Просмотры: {{$post->hits}}</span>
		</li>
	</ul>
</div>