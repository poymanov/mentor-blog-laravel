<div class="content-layout">
	<div class="heading">
		<h1>{{ $header }}</h1>

	</div>
	<div class="blog-content">
		<ul class="posts__list">
			@foreach($posts as $post)
				<li class="posts__item">
					<div class="posts__date">
						<div class="posts__date-text">
							{{ date('d M', strtotime($post->created_at)) }}
						</div>
					</div>
					@include('posts._info')
					<div class="posts__content">
						<div class="posts__text">
							<h1 class="posts__header">
								<a href="{{ route('users.showPost',['name'=>$post->user->name,'id'=>$post->id]) }}" class="posts__header_link">
									{{ $post->title }}
								</a>
								@if (Auth::user())
									@if (Auth::user()->id == $post->user->id)
										<a href="{{ route('users.editPost',['id' => $post->id]) }}" class="posts__header_edit"></a>
									@endif
								@endif
								</h1>
							<p>{!! $post->text !!}</p>
						</div>
						<div class="posts__footer">
							<a href="{{ route('users.showPost',['name'=>$post->user->name,'id'=>$post->id]) }}" class="posts__link">Далее</a>
						</div>
					</div>
				</li>
			@endforeach
		</ul>
		@if ($posts->lastPage() > 1)
			<ul class="pagination__list">
					@for ($i = 1; $i <= $posts->lastPage(); $i++)
							<li class="pagination__item {{ ($posts->currentPage() == $i) ? ' pagination__item-active' : '' }}">
									<a href="{{ $posts->url($i) }}" class="pagination__link">{{ $i }}</a>
							</li>
					@endfor
			</ul>
		@endif
	</div>
</div>