@extends('layouts.master')
@section('title',$post->title)
@section('content')
	<div class="content">
		<div class="content-layout">
			<div class="blog-content">
				<ul class="posts__list posts__list-singlepost">
					<li class="posts__item posts__item-singlepost">
						<div class="posts__date">
							<div class="posts__date-text">
								{{ date('d M', strtotime($post->created_at)) }}
							</div>
						</div>
						@include('posts._info')
						<div class="posts__content">
							<div class="posts__text">
								<h1 class="posts__header">{{ $post->title }}</h1>
								{!! $post->text !!}
							</div>
						</div>
					</li>
				</ul>
				<div class="post__footer">
					<div class="post__footer-content">
						<div class="post__footer-header">Другие записи</div>
						<ul class="post__footer-list">
							@foreach($randomPosts as $item)
								<li class="post__footer-item">
									<a href="{{ route('users.showPost',['name'=>$post->user->name,'id'=>$item->id]) }}" class="post__footer-link">{{ $item->title }}</a>
								</li>             
							@endforeach             	             	
						</ul>
					</div>
					<div class="post__footer-content">
						<div class="post__footer-header">Свежие записи блога</div>
						<ul class="post__footer-list">
							@foreach($lastPosts as $item)
								<li class="post__footer-item">
									<a href="{{ route('users.showPost',['name'=>$post->user->name,'id'=>$item->id]) }}" class="post__footer-link">{{ $item->title }}</a>
								</li>             
							@endforeach 
						</ul>
					</div>
				</div>	
				<div class="new-comment">          
				<h2 class="new-comment__header">Оставить комментарий</h2>
					@if (Auth::user())
						<form class="new-comment__form" method="post" action="{{ route('users.postComment') }}">
							{!! csrf_field() !!}
							<input type="hidden" name="post_id" value="{{ $post->id }}">
							<div class="new-comment__wrapper new-comment__wrapper_textarea">              
								<textarea name="comment_text" placeholder="Здесь будет интересный комментарий :)" class="new-comment__textarea">{{ old('comment_text') }}</textarea>
								@if ($errors->has('comment_text'))
									<div class="form__error">{{ $errors->first('comment_text') }}</div>
								@endif
								<div class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA_SITEKEY') }}"></div>
									@if ($errors->has('g-recaptcha-response'))
										<div class="form__error">
											{{ $errors->first('g-recaptcha-response') }}
										</div>
									@endif
							</div>             
							<input type="submit" value="Опубликовать" class="new-comment__submit">
						</form>
					@else
						<p><a href="/login/">Login</a> or <a href="/register/">register</a> to post comment.</p>
					@endif
				</div>
				
				@if ($post->comments)
					<ul id="comments" class="comments__list">
						@foreach($post->comments as $comment)
							@if (!$currentUser && $comment->active == 0 && Auth::user() != $comment->user)
								@continue
							@endif
							@if (Auth::user() == $comment->user && $comment->active == 0)
								<li class="comments__item comments__item_notvalid">
							@else
								<li class="comments__item">
							@endif
								<div class="comments__avatar"></div>
								<div class="comments__author">
									{{ $comment->user->name }}
									@if (Auth::user() == $comment->user && $comment->active == 0)
										(на модерации)
									@endif
								</div>
								<div class="comments__text">{{ $comment->comment_text }}</div>
								@if ($currentUser)
									<div class="comments__footer">
										<div class="comments__footer_right">
											@if ($comment->active == 0)
												<a href="{{ route('users.activateComment',['id' => $comment->id]) }}" class="comments__link comments__link_resolve">Опубликовать</a>
											@endif
											<a href="{{ route('users.deleteComment',['id' => $comment->id]) }}" class="comments__link comments__link_reject">Удалить</a>
										</div>
									</div>
								@endif
							</li>
						@endforeach
					</ul>
				@endif
		</div>
	</div>
@endsection