@extends('layouts.master')
@section('title',"Search - Blog")
@section('content')

<div class="content-layout">
  <div class="auth">
    <h1 class="header-content">Поиск</h1>
    <form method="post" class="form__search" method="post" action="{{ route('find.posts') }}">
      {!! csrf_field() !!}
      <div class="form__search-wrapper">
        <input type="text" name="search" placeholder="Искать..." class="form__search-input" value="{{ $query }}">
        <input type="submit" value="Искать" class="form__search-submit">
      </div>
      @if (isset($error))
        <div class="form__search-error">{{ $error }}</div>
      @endif
    </form>
    @if (isset($results))
      <h2 class="search__header">Результаты поиска</h2>
      <ul class="result__list">
        @foreach($results as $result)
          <li class="result__item">
            <a href="{{ route('users.showPost',['id'=>$result->id, 'name'=>$result->username]) }}" class="result__header">{{ $result->title }}</a>
          </li>
        @endforeach
      </ul>
    @endif
  </div>
</div>

@endsection