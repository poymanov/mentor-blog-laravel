<!DOCTYPE html>
<html>
	@include('admin.head')
	<body>
		@include('admin.header')
		<div class="page-content">
    	<div class="row">
    		@include('admin.sidebar')
    		@yield('content')
    	</div>
    </div>
	@include('admin.footer')
	</body>
</html>