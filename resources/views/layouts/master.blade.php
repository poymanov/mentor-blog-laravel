<!DOCTYPE html>
<html lang="ru-RU">
  @include('include.head')
  <body>
    <div class="wrapper">
      <div class="main-content">
        @include('include.header')
        @include('include.nav')
        <main class="main">
          <div class="content">
            @yield('content')
          </div>
        </main>
      </div>
    </div>
     @include('include.footer')
  </body>

</html>